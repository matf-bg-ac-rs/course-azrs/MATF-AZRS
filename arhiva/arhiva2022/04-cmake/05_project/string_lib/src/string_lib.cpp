#include "string_lib.hpp"

std::vector<std::string> split(const std::string& text, const char delimiter)
{
    std::vector<std::string> words;

    std::string word;
    for(auto c : text){
        if(c == delimiter){
            if(!word.empty()){
                words.push_back(word);
                word.clear();
            }
        }
        else{
            word.push_back(c);
        }
    }

    if(!word.empty()){
        words.push_back(word);
    }

    return words;
}