# Conan

`Conan` je alat za upravljanje paketima koji pojednostavljuje proces dodavanja i održavanja biblioteka u C++ projektima. Svako ko radi na C++ projektima gde su potrebne eksterne biblioteke ili zavisnosti može imati koristi od korišćenja Conana.

Recimo da razvijate C++ aplikaciju koja koristi neku spoljnu biblioteku, kao što je Boost C++ Libraries ili Google Test. Kada želite dodati ovu biblioteku u svoj projekat, morate se suočiti sa nekoliko izazova:

1. **Preuzimanje i Postavljanje Biblioteke:**
   - **Bez Conana:** Morate ručno preuzeti biblioteku, ekstrahovati je, postaviti u odgovarajući direktorijum u svom projektu.
   - **Sa Conanom:** Conan automatski preuzima i postavlja biblioteku kada definišete njen zahtev u `conanfile.txt` ili `conanfile.py`.

2. **Održavanje Verzija:**
   - **Bez Conana:** Morate pratiti verzije svake biblioteke, ažurirati ih ručno i prilagođavati vaš projekat.
   - **Sa Conanom:** Možete lako specificirati željenu verziju biblioteke u vašem Conan receptu, omogućavajući lako ažuriranje.

3. **Deljenje Projekta sa Drugima:**
   - **Bez Conana:** Morate deliti uputstva o postavljanju i verzionisanju biblioteka koje koristi vaš projekat.
   - **Sa Conanom:** Ostali članovi tima ili saradnici mogu lako reprodukovati vaš projekat sa svim zavisnostima korišćenjem jednostavnih Conan komandi.

Korišćenje Conana omogućava vam da se fokusirate na pisanje koda i razvoj vaše aplikacije, dok alat automatski rešava izazove upravljanja zavisnostima. Spisak i opis svih biblioteka možete naći na sajtu [Conan Center](https://conan.io/center).

## Instalacija

Ukoliko imate instaliran `python3` i `pip3` dovoljno je pokrenuti komande:

```bash
pip3 install conan
conan profile detect
```

Komanda `pip3 install conan` se koristi za instalaciju Conana, alata za upravljanje paketima u C++, putem Python paketnog menadžera pip.

Komanda `conan profile detect` se koristi za automatsko otkrivanje i generisanje profila (konfiguracije) sistema na kojem se Conan izvršava, uključujući informacije o kompajleru, arhitekturi i postavkama sistema.

## 01_compressor

Na [zvaničnom GitHub repozitorijumu](https://github.com/conan-io/examples2/tree/main/tutorial/) možete pronaći još primera i uputstava za upotrebu Conana u različitim scenarijima. Naš primer je prilagođena verzija [ovog primera sa repozitorijuma](https://github.com/conan-io/examples2/tree/main/tutorial/consuming_packages/simple_cmake_project).

Primer uzme string, kompresuje ga korišćenjem [`zlib`](https://conan.io/center/recipes/zlib?version=1.2.11) biblioteke i prikaže nam veličinu sirovog stringa, kompresovanog stringa i verziju zlib biblioteke koju koristimo.

Prva stvar koju moramo definisati je niz zavisnosti u našem programu. U korenom direktorijumu projekta napravićemo fajl `conanfile.txt` sa narednim sadržajem:

```conan
[requires]
zlib/1.2.11

[generators]
CMakeDeps
CMakeToolchain
```

U sekciji `requires` definišete koje vam sve biblioteke trebaju. Piše se u formatu `ime_biblioteke/verzija_biblioteke` i može se navesti proizvoljan broj biblioteka u nizu, svaka biblioteka u novom redu. Zaključavanje na određenu verziju paketa umesto korišćenja najnovije verzije pruža stabilnost i doslednost u razvoju softvera. Ovo sprečava neočekivane promene u zavisnostima i omogućava preciznu reprodukciju okoline, čime se smanjuje rizik od problema vezanih za kompatibilnost ili neočekivane promene u funkcionalnosti paketa.

U sekciji `generators` definišemo koje build sisteme želimo da integrišemo sa našim alatima. Da bi se biblioteke jednostavno linkovale korišćenjem `cmake` moramo dodati `CMakeDeps` i `CMakeToolchain` kao opcije. Opcije `CMakeDeps` i `CMakeToolchain` u generatorima Conana omogućavaju generisanje CMake datoteka (`Find<Package>.cmake` i `conan_toolchain.cmake`) koje olakšavaju integraciju sa CMake sistemom, obezbeđujući automatsko otkrivanje zavisnosti i podešavanje alatnog lanca za izgradnju projekta.

Ako hoćemo da nam se generišu ovi fajlovi, moramo pozvati narednu komandu u korenom direktorijumu projekta:

```bash
conan install . --output-folder=build --build=missing
```

- `.`: Označava trenutni direktorijum kao mesto gde se nalazi Conan recept, odnosno `conanfile.txt` ili `conanfile.py`. Conan će koristiti ovaj recept za identifikaciju i instalaciju zavisnosti.
- `--output-folder=build`: Postavlja izlazni direktorijum gde će biti smešteni rezultati instalacije, u ovom slučaju, gde će biti smešteni svi generisani fajlovi potrebni za izgradnju projekta. U ovom primeru, to je direktorijum build.
- `--build=missing`: Ova opcija govori Conanu da treba da izgradi (kompajlira) bilo koje zavisnosti koje nisu dostupne u lokalnom kešu. Ako je zavisnost već instalirana i dostupna u kešu, Conan će je koristiti iz keša umesto ponovnog kompajliranja. Ovo ubrzava proces instalacije, posebno kada se radi sa istim projektom na različitim mestima ili sistemima.

Pozivanjem ove komande se generisalo mnogo fajlova u direktorijumu `build`, ali oni koji su nam najbitniji su `conan_toolchain.cmake` i svi fajlovi oblika `Find*.cmake`. Ako pokrenemo narednu bash komandu, možemo videti kako `cmake` naziva sve biblioteke koje smo skinuli:

```bash
ls build | egrep 'Find.*?\.cmake' | sed -e 's/Find\([^.]*\).cmake/\1/'
```

izlaz komande:

```output
ZLIB
```

Važno je pogledati kako se biblioteke zovu, jer `cmake` razlikuje mala i velika slova, a nije unapred definisana notacija koja se koristi za imenovanje `conan` biblioteka. Primetite da se u `conanfile.txt` koriste sva mala slova, a biblioteka je imenovana svim vekikim slovima. Kada vidimo kako se biblioteke tačno zovu, ovu informaciju sada možemo da iskoristimo da napišemo `CMakeLists.txt`:

```cmake
cmake_minimum_required(VERSION 3.15)
project(compressor C)

find_package(ZLIB REQUIRED)

add_executable(${PROJECT_NAME} src/main.c)
target_link_libraries(${PROJECT_NAME} ZLIB::ZLIB)
```

Iz priloženog koda se da videti da se biblioteke dodate conanom uključuju kao strane biblioteke korišćenjem funkcije `find_package`, a kao argument prosledimo ime biblioteke.

Da bi završili izgradnju programa moramo da preko `cmake` komande završimo kompilaciju našeg projekta:

```bash
cd build
cmake .. -DCMAKE_TOOLCHAIN_FILE=conan_toolchain.cmake -DCMAKE_BUILD_TYPE=Release
cmake --build .
```

Opcijom `CMAKE_TOOLCHAIN_FILE` prosleđujemo putanju do `conan_toolchain.cmake` fajla koji je `conan` generisao. Treba naglasiti da kada se prosledi ta opcija, ako ne prosledimo koji je `CMAKE_BUILD_TYPE` izgradnja može da pukne.

Sada kad pokrenemo `./compressor` dobijemo naredni izlaz:

```output
Uncompressed size is: 233
Compressed size is: 147
ZLIB VERSION: 1.2.11
```

Ovo znači da smo uspešno izgradili projekat! Bash skripta koja samostalno radi izgradnju projekta može se naći u fajlu `build.sh`.

## Find i Toolchain

Fajlovi oblika `Find*.cmake` u nazivu u okviru `CMake` sistema su moduli za pretragu (Find modules) koje CMake koristi za pronalaženje i konfigurisanje spoljnih zavisnosti, biblioteka ili alata. Na primer, `FindBoost.cmake` bi se koristio za pronalaženje i konfigurisanje Boost biblioteke. U `CMakeLists.txt` bismo napisali `find_package(Boost REQUIRED)`, i `cmake` bi onda pretražio svoje podrazumevane putanje da bi uključio fajl koji se zove `FindBoost.cmake`. Ovi fajlovi omogućavaju CMake-u da automatizovano otkrije putanje, opcije kompajlera i druge informacije neophodne za uspešno povezivanje sa spoljnim resursima.

`conan_toolchain.cmake` je fajl koji generiše Conan alat za upravljanje paketima kako bi pojednostavio integraciju sa CMake sistemom. Ovaj fajl sadrži podešavanja za alatni lanac (toolchain) koji je specifičan za Conan, uključujući putanje do zavisnosti i postavke kompajlera. Kada se koristi, ovaj fajl automatski prilagođava CMake konfiguraciju kako bi uključio informacije koje su preuzete od Conana prilikom instalacije zavisnosti. To olakšava rad sa spoljnim bibliotekama i omogućava konzistentnost u razvojnom okruženju. Sveukupno, `conan_toolchain.cmake` igra ključnu ulogu u integrisanju Conana sa CMake-om, čineći upravljanje zavisnostima u C++ projektima jednostavnijim i efikasnijim.

## QtCreator

Često se dešava da preko terminala uspešno kompilirate i pokrenete projekat, i onda kada isti projekat pokrenete preko `QtCreator` editora build pukne u fazi generisanja `CMake` datoteka, i u `CMakeLists.txt` se žali na `find_package` komandu. To se dešava zato što Qt ne zna da vi koristite `conan` za dohvatanje biblioteka, a u `CMakeLists.txt` to nigde ne piše. Pošto sam `CMakeLists.txt` ne treba menjati, preostaje da `QtCreator` izmenimo tako da ga koristi. Najjednostavniji način da to postignemo je da preusmerimo koji direktorijum se koristi za izgradnju projekta.

Ako smo preko terminala jednom uspešno pokrenuli program, treba iskoristiti taj napravljeni `build` direktorijum i usmeriti `QtCreator` da nadalje unutar njega radi build pritiskom na čekić dugme. Prvo izbrišite `CMakeLists.txt.user` fajl iz svog direktorijuma. Kada ponovo pokušate da otvorite projekat preko `QtCreator` editora, pošto je ovaj fajl izbrisan on će ponovo otvoriti `Configure Project` prozor.

![Configure Project](images/configure_project.png)

Bez da išta menjam, pošto se build direktorijum nalazi na istom nivou gde se nalazi i koreni `CMakeLists.txt`, `QtCreator` mi ga je automatski dodao pod sekciju `Imported Kits` (na slici, videti jedini štiklirani kit). Kada bih ovakvo podešavanje prihvatio, on bi automatski od sad buildovao unutar tog direktorijuma, koristeći postavke koje su beć bile. **NAPOMENA** Da bi ovo radilo, `build` mora da je već napravljen i kompiliran. Ne brisati ga!

Ukoliko želite komplikovanije podešavanje:

```bash
build
├── Debug
│   ├── CMakeCache.txt
│   ├── CMakeFiles
│   ├── cmake_install.cmake
│   ├── Makefile
│   └── server
└── Release
    ├── CMakeCache.txt
    ├── CMakeFiles
    ├── cmake_install.cmake
    ├── Makefile
    └── server
```

Moraćete ručno da dodate putanje. Prvo kompilirati jednom da postoji u formatu koji želite, potom izbrisati `CMakeLists.txt.user` fajl, a potom ručno namestiti da izgleda ovako:

![Configure Project](images/configure_project_multi.png)

Sada na raspolaganju imate različite verzije projekta po potrebi.

## 02_pcaptest

Kao i prošli program, i za ovaj ćemo koristiti `conan` da nam vodi računa o stranim bibliotekama. Sadržaj `conanfile.txt`:

```conan
[requires]
pcapplusplus/23.09

[generators]
CMakeDeps
CMakeToolchain
```

Ovaj put ćemo želeti da kompiliramo program u `Debug` modu. Prvo ćemo iskoristiti `conan` da nam dovuče pakete:

```bash
conan install . --output-folder=build --build=missing --settings=build_type=Debug
```

Dodali smo opciju `--settings=build_type=Debug` da bi naglasili da se svi paketi kompiliraju u `Debug` modu. Podrazumevano je `Release` ako se opcija ne postavi. **NAPOMENA** Ukoliko za `conan` podesite jedan `build_type` a pri pozivanju `cmake` postavite drugačiji, u fazi linkovanja će pucati kompilacija. Vodite računa da vam se uvek poklapaju.

Sada kad pogledamo `build` direktorijum, videćemo da nijedan nema ime `Find*.conan`. Ako on ne postoji onda mora postojati `*Config.cmake` ili `*-config.cmake`. Modifikovaćemo komandu od malo pre da bismo našli kako se zove biblioteka koju smo skinuli:

```bash
ls build | egrep '(Find.*?|.*?(-c|C)onfig)\.cmake' | sed -e 's/\.cmake//g' | sed -e 's/^Find//g' | sed -e 's/\(-c\|C\)onfig$//g'
```

Izlaz:

```output
libpcap
PcapPlusPlus
```

Sada znamo da se naša biblioteka zove `PcapPlusPlus`.
Takođe vidimo da postoji biblioteka `libpcap` koju nismo naveli u `conanfile.txt`, ali to je verovatno zavisnost potrebna za
`PcapPlusPlus` pa je svakako imamo na raspolaganju i ne moramo je koristiti ako nam ne treba.
Ovako izgleda `CMakeLists.txt`:

```cmake
cmake_minimum_required(VERSION 3.12)
project(pcaptest)


set(CMAKE_CXX_STANDARD 17)
# popen()/pclose() are not C++ standard
set(CMAKE_CXX_EXTENSIONS ON)

add_executable("${PROJECT_NAME}" main.cpp)

find_package(PcapPlusPlus REQUIRED)

message(STATUS "Include dirs: ${PcapPlusPlus_INCLUDE_DIRS}")
message(STATUS "Libraries: ${PcapPlusPlus_LIBRARIES}")

target_include_directories(${PROJECT_NAME} PUBLIC ${PcapPlusPlus_INCLUDE_DIRS})
target_link_libraries("${PROJECT_NAME}" PUBLIC PcapPlusPlus::PcapPlusPlus)

file(
    COPY ${CMAKE_CURRENT_SOURCE_DIR}/input.pcap
    DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
)
```

Svaki put kada koristimo `conan` da dovučemo biblioteke, on nam automatski generiše 2 promenljive: `<package>_INCLUDE_DIRS` i `<package>_LIBRARIES`. Ukoliko imamo problem sa linkovanjem, uvek možemo porveriti njihove vrednosti i proveriti da li se sve poklapa sa našim očekivanjima. Na primer, ako u `main.cpp` fajlu dodamo `#include "PcapFileDevice.h"` kompajler će se žaliti da ovaj heder ne postoji iako u dokumentaciji znamo da ima. Kada proverimo izlaz komande `cmake` vidimo ovakvu poruku (ova poruka se ne dobija automatski, nego jer smo dodali `message` komande u `CMakeLists.txt`):

```output
-- Include dirs: /home/petar/.conan2/p/b/pcapp2c9f3c6014eb6/p/include
-- Libraries: PcapPlusPlus::PcapPlusPlus
```

Kada bih pokrenuo `ls /home/petar/.conan2/p/b/pcapp2c9f3c6014eb6/p/include`, dobijam izlaz:

```ls
pcapplusplus
```

Dakle, svi heder fajlovi mi se nalaze u direktorijumu `pcapplusplus`. Ako bih promenio zaglavlje na `#include "pcapplusplus/PcapFileDevice.h"`, kompilacija bi prošla.