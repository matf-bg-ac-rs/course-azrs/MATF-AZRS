#pragma once

#include <pqxx/pqxx>
#include <string>
#include <vector>

using user_t = std::pair<size_t, std::string>;

class Database {
public:
    static Database& global();
    inline bool isConnectionOpen() const {
        return connection.is_open();
    }

    void createUserTable();
    std::vector<user_t> getUsers();

private:
    Database();
    Database(const Database&) = delete;
    Database& operator=(const Database&) = delete;
    ~Database() = default;

    pqxx::result executeQuery(const std::string& query);
    pqxx::connection connection;
};