#include "server.hpp"
#include "database.hpp"

Server& Server::global()
{
    static Server instance;
    return instance;
}

void Server::listen(const char* address, int port)
{
    server.Get("/", [](const httplib::Request&, httplib::Response& res) {
        auto result = Database::global().getUsers();

        std::stringstream html;
        html << "<html><body><table border='1'><tr><th>ID</th><th>Name</th></tr>";
        for (auto& [id, name] : result) {
            html << "<tr><td>" << id << "</td><td>" << name << "</td></tr>";
        }
        html << "</table></body></html>";

        res.set_content(html.str(), "text/html");
    });

    server.listen(address, port);
}

Server::Server()
{
    Database::global().createUserTable();
    std::cout << "Table 'user' created successfully." << std::endl;
}