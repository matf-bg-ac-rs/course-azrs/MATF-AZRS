#include "database.hpp"

#include <iostream>
#include <iterator>
#include <numeric>

Database& Database::global()
{
    static Database instance;
    return instance;
}

pqxx::result Database::executeQuery(const std::string& query)
{
    if (!isConnectionOpen()) {
        return {};
    }
    pqxx::work txn(connection);
    pqxx::result result = txn.exec(query);
    txn.commit();
    return result;
}

void Database::createUserTable()
{
    executeQuery(R"(
        CREATE TABLE IF NOT EXISTS "user" (
            id SERIAL PRIMARY KEY,
            name VARCHAR(255)
        )
    )");
}

std::vector<user_t> Database::getUsers()
{
    auto result_db = executeQuery("SELECT * FROM \"user\"");
    std::vector<user_t> results;
    for(const auto& row : result_db){
        results.push_back({row["id"].as<size_t>(), row["name"].as<std::string>()});
    }

    return results;
}

Database::Database()
{
    if (isConnectionOpen()) {
        std::cout << "Connected to database successfully." << std::endl;
    } else {
        std::cerr << "Failed to connect to database." << std::endl;
        std::exit(EXIT_FAILURE);
    }
}