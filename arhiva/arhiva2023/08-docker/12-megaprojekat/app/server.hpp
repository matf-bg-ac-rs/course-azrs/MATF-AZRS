#pragma once

#include <httplib.h>

class Server {
public:
    static Server& global();

    void listen(const char* address, int port);

private:
    Server();
    Server(const Server&) = delete;
    Server& operator=(const Server&) = delete;
    ~Server() = default;

    httplib::Server server;
};