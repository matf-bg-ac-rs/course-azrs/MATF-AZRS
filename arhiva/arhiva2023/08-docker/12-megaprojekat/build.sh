#!/bin/bash

set -e

# Set path to source directory
BASEDIR=$(dirname "$0")
# Set default build type to "Release" if not provided as a parameter
BUILD_TYPE=${1:-Release}

# Parse command-line options
while getopts ":b:d:" opt; do
  case $opt in
    b)
      BUILD_TYPE="$OPTARG"
      ;;
    d)
      BASEDIR="$OPTARG"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# After all arguments are parsed, we do the actual work
rm -rf build

conan install $BASEDIR --output-folder=build --build=missing --settings=compiler.cppstd=20 --settings=build_type="$BUILD_TYPE" 
cd build
cmake $BASEDIR -DCMAKE_TOOLCHAIN_FILE=conan_toolchain.cmake -DCMAKE_BUILD_TYPE="$BUILD_TYPE"
cmake --build .