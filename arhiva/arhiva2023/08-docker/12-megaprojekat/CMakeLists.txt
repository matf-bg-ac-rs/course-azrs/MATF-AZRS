cmake_minimum_required(VERSION 3.14)
project(megaprojekat LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)

find_package(libpqxx REQUIRED)
find_package(httplib REQUIRED)

add_executable(
    ${PROJECT_NAME} app/main.cpp
    app/database.cpp app/database.hpp
    app/server.cpp app/server.hpp
)

target_link_libraries(
    ${PROJECT_NAME} 
    PRIVATE ${libpqxx_LIBRARIES} ${httplib_LIBRARIES}
)

set(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++")
set(CMAKE_CXX_FLAGS "-static")