# BASH

## Uvod

Bash, skraćenica od 'Bourne-Again Shell', je moćna i veoma korisna komandna linija i skriptni jezik koji se često koristi na Unix baziranim operativnim sistemima.
On služi kao interfejs između korisnika i osnovnog operativnog sistema, omogućavajući vam da komunicirate sa računarom putem tekstualnih komandi.
Bash je izuzetno vredan jer pruža fleksibilan i efikasan način za izvođenje zadataka, automatizaciju procesa i upravljanje datotekama i konfiguracijama sistema.
Njegove sposobnosti za izradu skripti omogućavaju korisnicima da kreiraju prilagođena rešenja, čineći ga neophodnim alatom za sistem administratore, programere i sve one koji žele da optimizuju svoj radni tok u okruženju terminala.

## Osnovna sintaksa i struktura komandi

U ovoj temi ćemo razgovarati o osnovnoj strukturi Bash komandi i njenom značaju. Naučićemo kako se pravilno formiraju komande, uključujući opcije (flagove) i argumente, i kako se koristi pomoć pri komandama putem `--help` ili `man` komande.

Bash komande imaju osnovnu strukturu koja se sastoji od komande, opcija i argumenata. Opcije (ili flagovi) obično se označavaju sa '-' ili '--', dok argumenti predstavljaju objekte ili vrednosti koje komanda obrađuje. Da biste saznali više o komandi, koristite --help ili man komandu za prikaz dokumentacije. Ispravno formiranje komandi je ključno za uspešno izvršavanje zadataka u Bash-u.

#### Metakarakteri i Zamene (Wildcards)

Metakarakteri, kao što su `*` i `?`, koriste se za traženje i obradu datoteka koje odgovaraju određenom obliku naziva.

- `*` (asterisk) se koristi za pronalaženje nula ili više znakova. Na primer, `*.yaml` će pronaći sve YAML datoteke u trenutnom direktorijumu.
- `?` (upitnik) se koristi za pronalaženje tačno jednog znaka. Na primer, `file?.txt` će pronaći datoteke poput `file1.txt`, `file2.txt`, itd.

**Primer:**

Ako imamo datoteke u trenutnom direktorijumu:
```bash
.
├── config.yaml
├── data.yaml
└── image.jpg
```
Komanda `cat *.yaml` će ispisati sadržaj obe YAML datoteke (`config.yaml` i `data.yaml`), jer `*` zamenjuje sve nazive koje završavaju sa `.yaml`. Datoteka `image.jpg` se neće uzeti u obzir jer ne odgovara uzorku `*.yaml`.

### Skraćenice i Promenljive u Bash-u

Bash šel pruža niz korisnih skraćenica i promenljivih koje olakšavaju rad i praćenje procesa. Evo nekoliko čestih skraćenica i promenljivih:

1. `$$` - PID Trenutnog Procesa

    Skraćenica `$$` se koristi za dobijanje PID-a (Process ID) trenutnog Bash procesa. Ovo je korisno za praćenje ili upravljanje procesima.

    **Primer:**

    ```bash
    echo "PID trenutnog procesa: $$"
    ```

2. `$!` - PID Poslednjeg Pozadinskog Procesa

    Skraćenica `$!` se koristi za dobijanje PID-a poslednjeg pozadinskog procesa koji je pokrenut. Ovo je korisno kada želite znati PID procesa koji se izvršava u pozadini.

    **Primer:**

    ```bash
    neki_dug_proces &
    echo "PID poslednjeg pozadinskog procesa: $!"
    ```

3. `$?` - Status Izlaza Poslednjeg Procesa

    Skraćenica `$?`` se koristi za dobijanje statusa izlaza poslednjeg izvršenog procesa. Ako je proces uspešno završen, ova promenljiva će biti postavljena na 0. Inače, sadrži broj koji označava grešku.

    **Primer:**

    ```bash
    neki_komandni_niz
    if [ $? -eq 0 ]; then
        echo "Proces je uspešno završen."
    else
        echo "Proces je završio s greškom."
    fi
    ```

4. `$_` - Poslednji Argument Poslednje Komande

    Skraćenica `$_` se koristi za referenciranje poslednjeg prosleđenog argumenta u prethodnoj komandi. Ovo je korisno za brzu upotrebu poslednjeg argumenta.

    **Primer:**

    ```bash
    echo "Hello, World!"
    echo "Poslednji argument prethodne komande: $_"
    ```

5. `!!` - Prethodna Komanda

    Skraćenica `!!` se koristi za ponovno izvršavanje odmah prethodne komande. Ovo je korisno za brzo ponavljanje komande.

    **Primer:**

    ```bash
    echo "Hello, World!"
    !!
    ```

## Česte Bash komande

1. `cd` - Promena direktorijuma
   - `cd putanja/do/direktorijuma` - Promeni trenutni direktorijum u određeni direktorijum.
   - `cd ..` - Vratite se u roditeljski direktorijum.
   - `cd -` - Vratite se u direktorijum u kojem ste se prethodno nalazili.

2. `ls` - Lista datoteka i direktorijuma
   - `ls` - Prikazuje sadržaj trenutnog direktorijuma.
   - `ls -l` - Detaljni prikaz sa dodatnim informacijama (veličina, dozvole, vlasnik).
   - `ls -a` - Prikazuje skrivene datoteke i direktorijume.

3. `pwd` - Trenutni direktorijum
   - `pwd` - Prikazuje punu putanju trenutnog direktorijuma.

4. `touch` - Kreiranje prazne datoteke
   - `touch ime_datoteke` - Kreira praznu datoteku sa određenim imenom.

5. `mkdir` - Kreiranje direktorijuma
   - `mkdir ime_direktorijuma` - Kreira novi direktorijum sa određenim imenom.
   - `mkdir -p putanja/novog/direktorijuma` - Rekursivno kreira sve potrebne direktorijume.

6. `rm` - Brisanje datoteka
   - `rm ime_datoteke` - Briše određenu datoteku.
   - `rm -r ime_direktorijuma` - Briše direktorijum i sve njegove sadržaje rekurzivno.
   - `rm -rf ime_direktorijuma` - Briše direktorijum i sve njegove sadržaje rekurzivno, i nastavlja dalje čak i ako ne uspe, ne pita korisnika za potvrdu.

7. `rmdir` - Brisanje praznih direktorijuma
   - `rmdir ime_direktorijuma` - Briše prazan direktorijum.

8. `mv` - Premestanje i preimenovanje datoteka/direktorijuma
   - `mv stari_naziv novi_naziv` - Premesti ili preimenuj datoteku ili direktorijum.

9. `cp` - Kopiranje datoteka/direktorijuma
   - `cp izvor destinacija` - Kopira datoteku ili direktorijum sa izvora na odredište.
   - `cp -r izvor destinacija` - Rekurzivno kopiranje

## Obrada Teksta u Bash-u

Obrada teksta je ključna veština u Bash programiranju, omogućavajući vam da manipulišete i analizirate tekstualne podatke. Evo nekoliko važnih alata i tehnika za obradu teksta:

1. `grep` - Pretraga teksta
   - Komanda `grep` omogućava pretragu teksta ili datoteka koristeći regularne izraze. Za korišćenje regularnih izraza, dodajte opciju `-E` (ili `--extended-regexp`) uz `grep`.
   - Primer: `grep -E "regex_pattern" datoteka`

2. `sed` - Stream Editor
   - `sed` je moćan alat za uređivanje teksta u toku obrade. Koristi se za zamenu, brisanje, ili transformaciju teksta na osnovu definisanih pravila. Regularni izrazi se često koriste za kompleksne transformacije.
   - Primer: `sed -E 's/regex_pattern/zamena/' datoteka`

3. `awk` - Procesiranje Teksta u Tabelarnom Formatu
   - `awk` je alat za procesiranje teksta u tabelarnom formatu. Omogućava analizu i manipulaciju podacima u kolonama i redovima.

4. Redirekcija i Cevi
   - Redirekcija (`>`, `>>`, `<`) omogućava usmeravanje ulazno/izlaznih tokova podataka između komandi i datoteka.
   - Cevi (`|`) omogućavaju da se izlaz jedne komande koristi kao ulaz za drugu, čime se stvara niz komandi za složeniju obradu podataka.

5. Manipulacija Teksta
   - Obrada teksta uključuje manipulaciju rečima i linijama, kao što su deljenje, spajanje, sortiranje i filtriranje podataka.

Ove tehnike su korisne za analizu log fajlova, izvlačenje informacija iz teksta, ili transformaciju podataka u različite formate. Razumevanje obrade teksta u Bash-u može biti ključno za automatizaciju zadataka i analizu podataka.

### Primeri Korišćenja `grep` i `sed`

Ove operacie ne moramo koristiti samo za tekstualne datoteke, već bilo koji izlaz bilo kog programa možemo pretraživati.

1. **Izdvajanje Komande iz Istorije:**

    ```bash
    history | grep "neka_komanda" | sed -E 's/^\s*^[0-9]*\s*//'
    ```

    Ova linija komandi će pretražiti istoriju komandi i izdvojiti sve linije koje sadrže "neka_komanda". Nakon toga, sed će ukloniti početni broj koji se obično nalazi u istoriji, ostavljajući samo komandu. Dobićete sve komande koje ste skoro kucali koje sadrže "neka_komanda".

2. Pronalaženje Trenutne Grane u Git-u:

    ```bash
    git branch | grep -E '^\s*\*\s*' | sed -E 's/^\s*\*\s*//'
    ```

    Ova naredba će koristiti `git branch` da prikaže listu grana u Git repozitorijumu, `grep` će pronaći red koji počinje sa " * " (primetite da smo morali da eskejpujemo zvezdicu sa bekslešom pošto yveydica ima specijalno značenje), što označava trenutnu granu, a `sed` će ukloniti " * " ostavljajući samo ime trenutne grane.
    
## Rad sa Promenljivima i Okruženjem

Rad sa promenljivama i okruženjem je važan deo Bash programiranja. Ovde ćemo istražiti kako definisati, koristiti i manipulisati promenljivama u Bash okruženju.

1. **Definisanje Promenljivih**

    Za definisanje promenljivih koristimo znak `=`. Na primer:

    ```bash
    ime="John"
    prezime="Doe"
    ```

2. **Korišćenje Promenljivih**

    Da biste pristupili vrednostima promenljivih, koristite znak `$`. Na primer:

    ```bash
    echo "Dobrodošli, $ime $prezime!"
    ```

3. **Unapređivanje Vrednosti Promenljivih**

    Možete promeniti vrednost promenljivih dodeljivanjem novih vrednosti. Na primer:

    ```bash
    ime="Jane"
    ```

4. **Brisanje Promenljivih**

    Za brisanje promenljive koristite komandu `unset`. Na primer:

    ```bash
    unset prezime
    ```

5. **Čuvanje Rezultata Komandi u Promenljivim**

    Rezultate komandi možete sačuvati u promenljivim koristeći `$()` sintaksu. Na primer:

    ```bash
    rezultat=$(ls -l)
    ```

6. **Razlika između `"` i `'`**

    Ukoliko nam je potrebno da tačno jedan argument bude niska koja u sebi ima razmake koriste se navodnici.

    Dvostruki navodnici (`"`) se koriste da se omogući zamena promenljivih i specijalnih znakova, dok jednostruki navodnici (`'`) zadržavaju sve znakove doslovno, bez zamene. 
    
    ```bash
    echo "Dobro jutro, $ime"
    ```
    Ovo će koristiti vrednost promenljive `ime` i ispisati `Dobro jutro, Jane`.
    
    ```bash
    echo 'Dobro jutro, $ime'
    ```
    Ovo doslovno ispisuje `Dobro jutro, $ime`.

7. **Lokalne i Globalne Promenljive**

    Promenljive su obično lokalne za trenutnu sesiju. Ako želite da promenljiva bude globalna, koristite `export`. Na primer:

    ```bash
    export GLOBALNA_PROMENLJIVA="Vrednost"
    ```

8. **Specijalne Promenljive**

    Bash sadrži mnoge specijalne:

    - **`$HOME`** - Putanja do Matičnog Direktorijuma Korisnika
    
    Ova promenljiva sadrži putanju do matičnog direktorijuma trenutnog korisnika.

    - **`$USER`** - Korisničko Ime

    Promenljiva `$USER` sadrži korisničko ime trenutnog korisnika.

    - **`GROUP`** - Korisnička Grupa

    Promenljiva `$GROUP` sadrži korisničku grupu trenutnog korisnika.

    - **`$PATH`** - Putanje Izvršnih Fajlova

    `$PATH` sadrži listu direktorijuma u kojima se Bash pretražuje za izvršne fajlove (komande).

    - **`$PWD`** - Trenutna Radna Direktorijum

    Ova promenljiva sadrži putanju do trenutnog radnog direktorijuma.

    - **`$SHELL`** - Putanja do Trenutnog Šela

    `$SHELL` sadrži putanju do trenutnog interaktivnog šela koji koristite.

    - **`$PS1`** - Promenljiva za Prikaz Unosa

    `$PS1` definiše format prikaza unosa (prompt) u šelu i može se prilagoditi.

    - **`$PS2`** - Promenljiva za Prikaz Unosa (Sekundarni)

    `$PS2` definiše format sekundarnog prikaza unosa u šelu.

    - **`$PS3`** - Promenljiva za Prikaz Unosa (Tercijarni)

    `$PS3` definiše format tercijarnog prikaza unosa u šelu (koristi se uz `select` komandu).

    - **`$PS4`** - Promenljiva za Prikaz Unosa (Četvrti)

    `$PS4` definiše format četvrtog prikaza unosa u šelu (koristi se uz `set -x` za debagovanje skripti).

    - **`$RANDOM`** - Nasumičan Broj

    `$RANDOM` sadrži nasumičan broj u opsegu od 0 do 32767 i često se koristi u skriptama za generisanje slučajnih vrednosti.

    - **`$HOSTNAME`** - Ime Računara

    Promenljiva `$HOSTNAME` sadrži ime trenutnog računara ili sistema.


9. **Čitanje Unosa od Korisnika**

    Za čitanje unosa od korisnika koristimo `read` komandu. Na primer:

    ```bash
    echo "Unesite svoje ime:"
    read ime_korisnika
    ```

10. **Promenljive u Skriptama**

    Promenljive su korisne u Bash skriptama za čuvanje privremenih i stalnih podataka.

11. **Upotreba Promenljivih u Okviru Skripti**

    Skripte mogu koristiti promenljive za čuvanje i razmenu informacija između komandi.

Rad sa promenljivama i okruženjem omogućava prilagodljivost i automatizaciju u Bash programiranju. Ove veštine su od suštinskog značaja za pisanje naprednijih skripti i manipulaciju podacima.

### Izvršavanje Skripti i Novi Šel

Kada pozovete Bash skriptu, to obično otvara novi šel proces u kojem će se izvršiti sadržaj skripte. To znači da sve promenljive i promene direktorijuma (npr. `cd`) unutar skripte će uticati samo na taj novi šel proces i neće uticati na trenutni šel proces iz kojeg ste pozvali skriptu.

Na primer, razmotrimo sledeći scenario sa skriptom `change_directory.sh`:

```bash
#!/bin/sh

cd /novi_direktorijum
pwd
```

Ako izvršite skriptu bez korišćenja komande `source`, to će otvoriti novi šel proces, promeniti direktorijum unutar tog procesa, prikazati trenutni direktorijum i zatim se vratiti u prethodni direktorijum:

```bash
$ ./change_directory.sh
/novi_direktorijum
$ pwd
/trenutni_direktorijum

```

Ako izvršite skriptu sa komandom `source`, to će izvršiti skriptu u trenutnom šel procesu, a promene u direktorijumu će se odraziti na trenutni šel proces:

```bash
$ source change_directory.sh
/novi_direktorijum
$ pwd
/novi_direktorijum
```

Korišćenje `source` omogućava da se promene unutar skripte prenesu u trenutni šel proces.

Ovo je važno razumeti prilikom izvršavanja skripti, jer utiče na to kako se promenljive, promene direktorijuma i druge izmene okoline odražavaju u trenutnom šel procesu.

### Značaj `set -e` u Bash Skriptama

Komanda `set -e` je korisna u Bash skriptama jer zaustavlja izvršavanje skripte odmah nakon detekcije greške prilikom izvršavanja bilo koje komande unutar skripte. Ovo je posebno važno za otkrivanje grešaka koje mogu dovesti do neželjenih posledica.

#### Primer Korišćenja `set -e`:

Razmotrimo sledeći primer Bash skripte koja koristi `wget` za preuzimanje fajla sa interneta:

```bash
#!/bin/bash

# Postavljanje opcije -e kako bi se zaustavilo izvršavanje na greškama
set -e

# URL za preuzimanje fajla
url="http://primer.com/fajl.zip"

# Preuzimanje fajla koristeći wget
wget "$url" -O preuzeti_fajl.zip

# Ovaj deo skripte nikada neće biti izvršen ako wget ne uspe
echo "Fajl je uspešno preuzet!"

# Unzipovanje preuzetog fajla u trenutni radni direktorijum
unzip preuzeti_fajl.zip

echo "Fajl je unzipovan u trenutni direktorijum!"
```

U ovom primeru, postavljanje `set -e` osigurava da će se izvršavanje skripte momentalno zaustaviti ako komanda `wget` ne uspe u preuzimanju fajla sa interneta. To sprečava izvođenje ostalih koraka u skripti ako ključna operacija ne uspe. Bez ove opcije, iako skidanje sa interneta ne uspe skripta će se nastaviti sa izvršavanjem.

## Strukture Kontrole

Strukture kontrole su ključni elementi u Bash programiranju koji omogućavaju upravljanje tokom izvršavanja skripti. Ove strukture omogućavaju uslovno izvršavanje komandi, ponavljanje određenih operacija i obradu grešaka. Evo pregleda nekoliko osnovnih struktura kontrole:

1. **Uslovna Struktura IF-THEN-ELSE**

Uslovna struktura `if-then-else` se koristi za izvršavanje određenih komandi pod određenim uslovima.

```bash
if [ uslov ]; then
    # Komande koje će se izvršiti ako je uslov tačan
else
    # Komande koje će se izvršiti ako uslov nije tačan
fi
```

2. **Petlje (Loops)**

Petlje se koriste za ponavljanje komandi više puta.
   
- **`for` Petlja**
    
    ```bash
    for var in lista; do
        # Komande koje će se izvršiti za svaku vrednost iz liste
    done
    ```

- **`while` Petlja**
    
    ```bash
    while [ uslov ]; do
        # Komande koje će se izvršiti dok je uslov tačan
    done
    ```

3. **Kontrola Izlaska**

Kontrola izlaska omogućava upravljanje završetkom izvršavanja skripte ili petlje.
   
- **`exit` Komanda**

    ```bash
    exit [kod_završetka]
    ```

- **`break` i `continue` Komande**

    - `break` se koristi za prekid petlje.
    - `continue` se koristi za preskakanje trenutne iteracije petlje i prelazak na sledeću.

Strukture kontrole su ključne za pisanje naprednih Bash skripti koje mogu obraditi različite situacije, uslovno izvršavati komande i ponavljati operacije kako bi se postigli željeni rezultati.

### Upravljanje Uslovima i Matematikom u Bash-u

Bash skripte često koriste uslovne izraze kako bi se donele odluke i izvršile komande na osnovu zadatih uslova. Evo kako se rukuju uslovima u Bash-u:

Bash koristi uslovne operatore za poređenje vrednosti i izračunavanje uslova. Najčešće korišćeni uslovni operatori su:

- **`-eq`** - Jednako (za brojeve)
- **`-ne`** - Različito (za brojeve)
- **`-lt`** - Manje od (za brojeve)
- **`-le`** - Manje ili jednako (za brojeve)
- **`-gt`** - Veće od (za brojeve)
- **`-ge`** - Veće ili jednako (za brojeve)
- **`=`** - Jednako (za stringove)
- **`!=`** - Različito (za stringove)

1. Upotreba uslova za brojeve:

```bash
if [ $broj -gt 10 ]; then
    echo "Broj je veći od 10."
fi
```

2. Upotreba uslova za stringove

```bash
if [ "$string1" = "$string2" ]; then
    echo "Stringovi su jednaki."
fi
```

3. Matematički Izrazi

Bash takođe može da se koristi za matematičke izraze. Da biste izračunali matematičke izraze, koristite dvostruki okrugle zagrade `(( ))`. Na primer:

```bash
rezultat=$((broj1 + broj2))
echo "Rezultat je: $rezultat"
```

4. Funkcije za Testiranje Postojanja

Bash takođe nudi brojne funkcije za testiranje postojanja direktorijuma ili datoteke. `test` komanda se često koristi za izračunavanje uslova. Sledeći primer koristi test komandu za proveru da li je fajl postoji:

```bash
if test -e "fajl.txt"; then
    echo "Fajl postoji."
fi
```

5 Kreiranje Funkcija u Bash-u

Funkcije su ključni deo Bash skriptiranja i omogućavaju vam da organizujete i ponovo koristite komande unutar vaših skripti. Funkcije se definišu koristeći ključnu reč `function` ili jednostavno navođenjem imena funkcije, otvorene zagrade `(`, parametara ako su potrebni, zatvorene zagrada `)`, i bloka koda u vitičastim zagradama `{ }`.

```bash
# Definisanje funkcije sa parametrima
function pozdrav {
    echo "Dobrodošli, $1!"
}

# Poziv funkcije sa argumentom
pozdrav "Korisnik"
```

U ovom primeru, funkcija `pozdrav` je kreirana sa jednim parametrom `$1`, koji predstavlja ime korisnika. Nakon definisanja funkcije, možete je pozvati sa argumentom (u ovom slučaju, "Korisnik"), i funkcija će ispisati poruku `Dobrodošli, Korisnik!`.


### Primer Korišćenja `find` Komande

Komanda `find` se može koristiti za pretragu fajlova i direktorijuma u određenom direktorijumu. Na primer, ako želite da pronađete sve fajlove sa ekstenzijom `.txt` unutar direktorijuma `/home/korisnik`, možete koristiti sledeću komandu:

```bash
find /var/logs -type f \( -name "*.log" -o -name "*.txt" \) -mtime +7 -size +1M
```

- `/var/logs` - Direktorijum u kojem se vrši pretraga.
- `-type f` - Uslov za pronalaženje samo fajlova (ne direktorijuma).
- `\( -name "*.log" -o -name "*.txt" \)` - Uslov za pronalaženje fajlova sa ekstenzijama .log ili .txt.
- `-mtime +7` - Uslov za pronalaženje fajlova starijih od 7 dana.
- `-size +1M` - Uslov za pronalaženje fajlova većih od 1MB.

## Konfiguracioni Fajl `.bashrc` u Bash-u

`.bashrc` je konfiguracioni fajl u Bash šelu koji se koristi za postavljanje različitih opcija i personalizaciju okoline za korisnika. Ovaj fajl se izvršava svaki put kada se otvori interaktivni šel sesija, kao što je otvaranje terminala. Umesto da postavljate sve konfiguracije direktno u `.bashrc`, možete koristiti ovaj fajl za učitavanje drugih konfiguracionih fajlova i organizaciju vaših podešavanja.

### Upotreba `.bashrc` Fajla

Da biste otvorili ili kreirali `.bashrc` fajl u vašem matičnom direktorijumu, možete koristiti svoj omiljeni tekst editor. Na primer:

```bash
vim ~/.bashrc
```

U `.bashrc` fajlu možete koristiti `source` komandu za učitavanje drugog konfiguracionog fajla, kao što je `custom_config.sh`. Ovo vam omogućava da organizujete sve vaše konfiguracije na jednom mestu:

```bash
# Učitavanje dodatnih konfiguracija iz custom_config.sh
source ~/custom_config.sh
```

Sve vaše specifične konfiguracije i podešavanja možete smestiti u `custom_config.sh` fajl, kao što su:

```bash
#!/bin/bash

# Dodavanje direktorijuma na PATH
export PATH=$PATH:$HOME/.local/bin

# Korisni aliasi
alias ll='ls -alF'
alias ..='cd ..'
alias grep='grep --color=auto'
alias currentbranch='git branch | grep \* | sed "s/* //"'

# Definisanje boja radi preglednosti
GREEN='\[\e[1;32m\]'
RESET='\[\e[0m\]'

# Postavljanje personalizovanog PS1 prompta sa bojama
PS1="${GREEN}\u@\h:${RESET}\w\$ "

# Postavljanje promenljive okoline
export MY_VARIABLE="Vrednost mog promenljivog"
```

Na ovaj način, `.bashrc` će samo učitati konfiguracije iz `custom_config.sh`, što čini `.bashrc` fajl preglednijim i organizovanijim.

Nakon dodavanja ovih konfiguracija i učitavanja `custom_config.sh` u `.bashrc`, možete ponovo pokrenuti vaš šel ili koristiti `source ~/.bashrc` komandu da biste primenili promene.