set(
    SOURCES
    src/string_lib.cpp
)

set(
    HEADERS
    inc/string_lib.hpp
)

add_library(string_lib ${SOURCES} ${HEADERS})

target_include_directories(
    string_lib
    PUBLIC "$(CMAKE_CURRENT_SOURCE_DIR)/inc"
)

add_executable(run_test test/test.cpp)
target_link_libraries(run_test PRIVATE string_lib)

add_test(
    NAME test_string_lib
    COMMAND run_test
)