# MATF-AZRS

## Teme

Spisak planiranih tema:

- Skript jezici (`bash`)
- Kontrola verzija (`git`)
- Dinamička analiza koda (`gdb`, `valgrind`)
- Sistemi za izgradnju paketa (`make`, `CMake`)
- Linteri (`clang-tidy`, `clang-format`)
- Upravljanje zavisnostima (`conan`)
- Isporuka u obliku kontejnera (`docker`)
- Kontinualna integracija i kontinualna isporuka (`GitLab CI/CD`)
- Infrastruktura u vidu koda (`terraform`)

## Snimci vežbi

Na ovom [linku](https://www.youtube.com/watch?v=it4lci7lKEQ&list=PLr6PFLAj4aAHw_-TeEJ-boMKUCJi0NxAK&ab_channel=MomirAdzemovic) se mogu pronaći prošlogodišnji snimci vežbi asistenta Momira Adžemovića.

## Predavanja

Profesor: Ivan Čukić ([link](http://poincare.matf.bg.ac.rs/~ivan/?content=azrs))

## Projekat

Informacije:

- Mora biti dodatak na neki projekat koji se radi u `C++`-u.
- Projekat na koji se primenjuju alati može da se radi u timu, ali pisanje izveštaja za projekat iz AZRS-a se radi samostalno.
- Nosi 30 poena.
- Ocenjuje se primena alata koji se obrađuju na predavanjima i vežbama.
- Mogu da se koriste alternative za alate neke namere. Primer: Ne mora da se koristi `gitflow` model grananja, ali u tom slučaju je potrebno postaviti referencu na model grananja ili ga ukratko opisati.

## Ispit

Informacije:

- Na ispitu može doći bilo koje pitanje ili zadatak iz teme koja je pokrivena na predavanjima ili vežbama.
- Ispit nosi 70 poena.
- Praktični i teorijski deo se rade zajedno.
- Bonus teme ne dolaze na ispit.
